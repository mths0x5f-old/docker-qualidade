# Imagem Docker para o ambiente do Qualidade

1. Primeiramente, clone este repositório em sua máquina.
2. Dentro da raiz deste repositório, crie a imagem do ambiente com este comando:
    ```bash
        docker build --tag=qualidade .
    ```
3. E depois disso, rode o container com este comando:
    ```bash
        docker run -p 8080:8080 -p 8787:8787 -p 9990:9990 -v deployments:/opt/jboss/wildfly/standalone/deployments/:rw -v adsl:/opt/adsl/:rw -it qualidade
    ```

Qualquer .war movido para o diretório `deployments` será carregado pelo WildFly. Qualquer arquivo editado no diretório `adsl` será refletido no container.

P.S.: Observe no último comando, que as portas 8787 e 9990 estão interligadas à sua máquina local, assim como a 8080. 