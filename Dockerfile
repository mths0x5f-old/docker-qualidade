FROM jboss/wildfly:10.1.0.Final

VOLUME /opt/adsl
VOLUME /opt/jboss/wildfly/standalone/deployments/

ADD standalone-custom.xml  /opt/jboss/wildfly/standalone/configuration/
ADD modules                /opt/jboss/wildfly/modules/

RUN /opt/jboss/wildfly/bin/add-user.sh admin admin --silent
CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-c", "standalone-custom.xml", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0", "--debug", "8787"]
